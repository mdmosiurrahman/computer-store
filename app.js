const loanElement = document.getElementById("loan");
const bankElement = document.getElementById("bank");
const workElement = document.getElementById("work");
const laptopsElement = document.getElementById("laptops");
const featureElement = document.getElementById("feature");
const descriptionElement = document.getElementById("description");
const buyElement = document.getElementById("buy");
const computerimageElement= document.getElementById("compterimage");


const baseUrl='https://noroff-komputer-store-api.herokuapp.com/';


let laptops = [];
let balance = 0;
let pay = 0;
let outstandingloan=0;
let buyCompueter=false;
let selectedLaptop=null;
let selectedPrice = 0;

document.getElementById("balance").innerText=balance;
//document.getElementById("outstandingLoan").innerText=outstandingloan;


fetch(baseUrl+'computers')
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops))

// iterate every laptop from api data list based on title
const addLaptopsToMenu = (laptops) =>{
    laptops.forEach(x => addLaptopToMenu(x));
    //laptopElement.innerText = laptops.title;
}

// here select laptop from various option and add to choice list
const addLaptopToMenu = (laptop) =>{
    if(laptop===undefined) return;
    laptopElement =document.createElement("option");
    laptopElement.value=laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
}

// after select the laptop(click your prefared laptop) based on title it will provide features what we want
const handleLaptopMenuChange = e => {
    selectedLaptop = laptops[e.target.selectedIndex];
    featureElement.innerText = selectedLaptop.specs[0].toLowerCase() + '\n ' +  selectedLaptop.specs[1].toLowerCase();   
    //descriptionElement.innerText = baseUrl+selectedLaptop.image;
    computerimageElement.src=baseUrl+selectedLaptop.image;
    document.getElementById("title").innerText = selectedLaptop.title;
    document.getElementById("description").innerText = selectedLaptop.description;
    document.getElementById("showprice").innerText = ' Price : ' +selectedLaptop.price + ' Kr';
    document.getElementById("buy").style ='display:block;';
    //selectedPrice = selectedLaptop.price;
}

//add money on pay section after every click
const handleWork = () =>{
    pay += 100;
    document.getElementById("pay").innerText=pay;

}

const handleBank = () =>{
    if(outstandingloan>0){
        outstandingloan=outstandingloan-pay*.1;
        balance += pay- pay*.1;
    }
    else{
        balance += pay;
    }
    
    pay=0;
    document.getElementById("pay").innerText=pay;
    document.getElementById("balance").innerText=balance;
    document.getElementById("outstandingLoan").innerText=Math.abs(outstandingloan);

}


const buyNow = () =>{
    if(selectedLaptop==null){
        return;
    }
    if(selectedLaptop.price>balance)
       {
           alert("You don't have sufficient Balance");
           return;
       }
    buyComputer=true;
    balance=balance-selectedLaptop.price;
    document.getElementById("balance").innerText=balance;
    alert("Thank you . Now this laptop belongs to you");

}


laptopsElement.addEventListener("change", handleLaptopMenuChange);
workElement.addEventListener("click", handleWork);
bankElement.addEventListener("click", handleBank);
buyElement.addEventListener("click", buyNow);

// experiment different way 
document.getElementById("loan").addEventListener("click", function(){
if(outstandingloan>0 && buyComputer==false)
{
    alert('You can not get loan before buying a compueter');
    rerturn;
}

let loan=prompt ("You can take loan more than double of your Balance ");  
if(loan!=null && loan>balance*2)
    {
        alert('You cannot take more than double of your Balance');
       return;
    }
    if(loan==null)
    {
        alert('You cannot take more than double of your Balance');
       return;
    }

    outstandingloan=loan;
    balance=balance+parseInt(outstandingloan);
    buyComputer=false;
    document.getElementById("balance").innerText=balance;
    document.getElementById("outstandingLoan").innerText=Math.abs(outstandingloan);

});

//repay loan
const repayLoan = () =>{
   
    if(outstandingloan>0){
        outstandingloan-=pay;
        pay=0;
        document.getElementById("pay").innerText=pay;
        document.getElementById("outstandingLoan").innerText=outstandingloan;
    }else{
        alert("you dont have any outsatanding loan");
    }
}

document.getElementById("repay").addEventListener("click", repayLoan);